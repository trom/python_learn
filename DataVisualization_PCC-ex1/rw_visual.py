import matplotlib.pyplot as plt

from random_walk import RandomWalk

# Keep making new walks, as long as the program is active
while True:
    # Make a random walk and plt the points
    rw = RandomWalk()
    rw.fill_walk()

    plt.scatter(rw.x_values, rw.y_values, c=rw.y_values, cmap=plt.cm.Reds, edgecolor='none', s=5)

    plt.show()

    keep_running = input("Make an other walk? (y/n): ")
    if keep_running == 'n':
      break
